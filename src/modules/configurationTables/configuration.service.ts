import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AdmitReason } from 'src/entity/admitReason.entity';
import { RoomCategory } from 'src/entity/roomCategory.entity';
import { RoomStatus } from 'src/entity/roomStatus.entity';
import { Repository } from 'typeorm';

/**
 * configuration service having all the business logics related to roomCategory, roomStatus, admitRason
 */
@Injectable()
export class ConfigurationService {
  /**
   * injecting all repositories for storing and retriving the data
   * @param roomCategoryRepository injecting roomCategoryRepository
   * @param roomStatusRepository injecting roomStatusRepository
   * @param admitReasonRepository injecting admitReasonRepository
   */
  constructor(
    @InjectRepository(RoomCategory)
    private roomCategoryRepository: Repository<RoomCategory>,
    @InjectRepository(RoomStatus)
    private roomStatusRepository: Repository<RoomStatus>,
    @InjectRepository(AdmitReason)
    private admitReasonRepository: Repository<AdmitReason>,
  ) {}

  /**
   * add method for room categories
   * @param data passing roomCategory
   * @returns saving tha data to roomCategory database
   */
  addRoomCategories(data: RoomCategory): Promise<RoomCategory> {
    return this.roomCategoryRepository.save(data);
  }

  /**
   * get method for room categories
   * @returns all info of room category
   */
  getAllRoomCategories(): Promise<RoomCategory[]> {
    return new Promise((res) => {
      setTimeout(() => {
        res(this.roomCategoryRepository.find());
      }, 3000);
    });
  }

  /**
   * add method for room status
   * @param data taking room status
   * @returns saving data into room status database
   */
  addRoomStatus(data: RoomStatus): Promise<RoomStatus> {
    return this.roomStatusRepository.save(data);
  }

  /**
   * get method for room staus
   * @returns all info about the room status
   */
  getAllRoomStatus(): Promise<RoomStatus[]> {
    return new Promise((res) => {
      setTimeout(() => {
        res(this.roomStatusRepository.find());
      }, 3000);
    });
  }

  /**
   * add method for admitted reason
   * @param data taking admitreason data
   * @returns saving the data to admit reason database
   */
  addAdmitReason(data: AdmitReason): Promise<AdmitReason> {
    return this.admitReasonRepository.save(data);
  }

  /**
   * get method for admit reason
   * @returns all info about the admit reason
   */
  getAllAdmitedreason(): Promise<AdmitReason[]> {
    return new Promise((res) => {
      setTimeout(() => {
        res(this.admitReasonRepository.find());
      }, 3000);
    });
  }
}
