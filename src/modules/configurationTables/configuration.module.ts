import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AdmitReason } from 'src/entity/admitReason.entity';
import { RoomCategory } from 'src/entity/roomCategory.entity';
import { RoomStatus } from 'src/entity/roomStatus.entity';
import { ConfigurationController } from './configuration.controller';
import { ConfigurationService } from './configuration.service';

@Module({
  imports: [TypeOrmModule.forFeature([RoomCategory, RoomStatus, AdmitReason])],
  controllers: [ConfigurationController],
  providers: [ConfigurationService],
})
export class ConfigurationModule {}
