import { Body, CacheKey, Controller, Get, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { AdmitReason } from 'src/entity/admitReason.entity';
import { RoomCategory } from 'src/entity/roomCategory.entity';
import { RoomStatus } from 'src/entity/roomStatus.entity';
import { ConfigurationService } from './configuration.service';

/**
 * configuration controller having all the api related to room status,room category,admit reason
 */
@ApiTags('Configuration')
@Controller('/configuration')
export class ConfigurationController {
  /**
   * injecting configuration service this having all businesss logice related to roomstatus, roomCategory, admitReason
   * @param configurationService injecting configuration service
   */
  constructor(private readonly configurationService: ConfigurationService) {}

  /**
   * add method for room categories
   * @param data passing roomCategory
   * @returns saving tha data to roomCategory database
   */
  @Post('/roomCategory')
  async addRoomCategories(@Body() data: RoomCategory): Promise<RoomCategory> {
    return await this.configurationService.addRoomCategories(data);
  }

  /**
   * add method for room status
   * @param data taking room status
   * @returns saving data into room status database
   */
  @Post('/roomStatus')
  async addRoomStatus(@Body() data: RoomStatus): Promise<RoomStatus> {
    return await this.configurationService.addRoomStatus(data);
  }

  /**
   * add method for admitted reason
   * @param data taking admitreason data
   * @returns saving the data to admit reason database
   */
  @Post('/admittedReason')
  async addAdmittedReason(@Body() data: AdmitReason): Promise<AdmitReason> {
    return await this.configurationService.addAdmitReason(data);
  }

  /**
   * get method for room staus
   * @returns all info about the room status
   */
  @Get('/roomStatus')
  @CacheKey('room_status')
  async getAllRoomStatus() {
    return this.configurationService.getAllRoomStatus();
  }

  /**
   * get method for room categories
   * @returns all info of room category
   */
  @Get('/roomCategory')
  @CacheKey('room_category')
  async getAllRoomCategories() {
    return this.configurationService.getAllRoomCategories();
  }

  /**
   * get method for admit reason
   * @returns all info about the admit reason
   */
  @Get('/admittedReason')
  @CacheKey('admit_reason')
  async getAllAdmitedreason() {
    return this.configurationService.getAllAdmitedreason();
  }
}
