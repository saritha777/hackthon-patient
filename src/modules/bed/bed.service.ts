import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BedInfo } from 'src/entity/bed.entity';
import { RoomCategory } from 'src/entity/roomCategory.entity';
import { RoomStatus } from 'src/entity/roomStatus.entity';
import { Repository } from 'typeorm';

/**
 * bed info service having all the business logics
 */
@Injectable()
export class BedInfoService {
  /**
   * injecting bedInfoRepository, roomCategoryRepository,roomStatusRepository
   * @param bedInfoRepository injecting bedInfoRepository
   * @param roomCategoryRepository injecting
   * @param roomStatusRepository
   */
  constructor(
    @InjectRepository(BedInfo)
    private bedInfoRepository: Repository<BedInfo>,
    @InjectRepository(RoomCategory)
    private roomCategoryRepository: Repository<RoomCategory>,
    @InjectRepository(RoomStatus)
    private roomStatusRepository: Repository<RoomStatus>,
  ) {}

  /**
   * add method for bed info
   * @param data taking bed info
   * @returns saving data to database
   */
  async addBedInfo(data: BedInfo): Promise<BedInfo> {
    const roomCategoryResult = await this.roomCategoryRepository.findOne({
      id: data.roomCategoryId,
    });
    if (!roomCategoryResult) {
      throw new HttpException('no data availabe', HttpStatus.BAD_REQUEST);
    } else {
      const roomStatusResult = await this.roomStatusRepository.findOne({
        id: data.roomStatusId,
      });
      if (!roomStatusResult) {
        throw new HttpException('no data availabe', HttpStatus.BAD_REQUEST);
      } else {
        const bedData = new BedInfo();
        bedData.roomCategory = roomCategoryResult;
        bedData.roomStatus = roomStatusResult;
        const newData = Object.assign(bedData, data);
        return await this.bedInfoRepository.save(newData);
      }
    }
  }

  /**
   * get all bed info
   * @returns all bed information
   */
  async getAllBedInfo(): Promise<BedInfo[]> {
    return await this.bedInfoRepository.find();
  }

  /**
   * update method for bedinfo
   * @param id taking id which we need to update
   * @param data taking bed info
   * @returns updated info
   */
  async updateBedInfo(id: number, data: BedInfo) {
    const roomCategoryResult = await this.roomCategoryRepository.findOne({
      id: data.roomCategoryId,
    });
    if (!roomCategoryResult) {
      throw new HttpException('no data availabe', HttpStatus.BAD_REQUEST);
    } else {
      const roomStatusResult = await this.roomStatusRepository.findOne({
        id: data.roomStatusId,
      });
      if (!roomStatusResult) {
        throw new HttpException('no data availabe', HttpStatus.BAD_REQUEST);
      } else {
        const bedData = new BedInfo();
        bedData.roomCategory = roomCategoryResult;
        bedData.roomStatus = roomStatusResult;
        const newData = Object.assign(bedData, data);
        return await this.bedInfoRepository.update({ id: id }, newData);
      }
    }
  }

  /**
   * delete method for info
   * @param id taking id which we need to delete
   * @returns deleted info
   */
  async deleteBedInfo(id: number) {
    return await this.bedInfoRepository.delete({ id: id });
  }
}
