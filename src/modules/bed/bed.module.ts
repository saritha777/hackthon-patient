import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BedInfo } from 'src/entity/bed.entity';
import { RoomCategory } from 'src/entity/roomCategory.entity';
import { RoomStatus } from 'src/entity/roomStatus.entity';
import { BedInfoController } from './bed.controller';
import { BedInfoService } from './bed.service';

@Module({
  imports: [TypeOrmModule.forFeature([BedInfo, RoomCategory, RoomStatus])],
  controllers: [BedInfoController],
  providers: [BedInfoService],
})
export class BedInfoModule {}
