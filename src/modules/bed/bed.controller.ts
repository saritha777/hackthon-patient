import {
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { CompoDecorators } from 'src/common/decorators/compodecorator';
import { BedInfo } from 'src/entity/bed.entity';
import { BedInfoService } from './bed.service';

/**
 * bedInfoController having all api related to bedInfo
 */
@ApiTags('BedInfo')
@Controller('/BedInfo')
export class BedInfoController {
  /**
   * Injecting bedInfo service for all business logics
   * @param bedInfoService Injecting bedInfo service
   */
  constructor(private readonly bedInfoService: BedInfoService) {}

  /**
   * add method for bed info
   * @param data taking bed info
   * @returns saving data to database
   */
  @Post()
  @CompoDecorators('admin', 'receptionist')
  addBedInfo(@Body() data: BedInfo): Promise<BedInfo> {
    return this.bedInfoService.addBedInfo(data);
  }

  /**
   * get all bed info
   * @returns all bed information
   */
  @Get()
  getAllBedInfo() {
    return this.bedInfoService.getAllBedInfo();
  }

  /**
   * update method for bedinfo
   * @param id taking id which we need to update
   * @param data taking bed info
   * @returns updated info
   */
  @Put('/id/:id')
  @CompoDecorators('admin', 'receptionist')
  updatebedInfo(@Param('id') id: number, @Body() data: BedInfo) {
    return this.bedInfoService.updateBedInfo(id, data);
  }

  /**
   * delete method for info
   * @param bedId taking id which we need to delete
   * @returns deleted info
   */
  @Delete('/:bedId')
  @CompoDecorators('admin', 'receptionist')
  deleteBedInfo(@Param('bedId') bedId: number) {
    return this.bedInfoService.deleteBedInfo(bedId);
  }
}
