import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AdmitReason } from 'src/entity/admitReason.entity';
import { BedInfo } from 'src/entity/bed.entity';
import { BedAllocation } from 'src/entity/bedAllocation.entity';
import { PatientInfo } from 'src/entity/patient.entity';
import { Repository } from 'typeorm';

/**
 * bed allocation service
 */
@Injectable()
export class BedAllocationService {
  /**
   * injecting all the repositories for saving and retriving the data
   * @param bedAllocationRepository injecting bedAllocationRepository
   * @param bedInfoRepository injecting bedInfoRepository
   * @param patientInfoRepository injecting patientInfoRepository
   * @param admitReasonRepository injecting admitReasonRepository
   */
  constructor(
    @InjectRepository(BedAllocation)
    private bedAllocationRepository: Repository<BedAllocation>,
    @InjectRepository(BedInfo)
    private bedInfoRepository: Repository<BedInfo>,
    @InjectRepository(PatientInfo)
    private patientInfoRepository: Repository<PatientInfo>,
    @InjectRepository(AdmitReason)
    private admitReasonRepository: Repository<AdmitReason>,
  ) {}

  /**
   * bed Allocation method
   * @param data taking bed allocation data
   * @returns all bedAllocation info
   */
  async bedAllocation(data: BedAllocation): Promise<BedAllocation> {
    const patientResult = await this.patientInfoRepository.findOne({
      id: data.patientId,
    });
    if (!patientResult) {
      throw new HttpException(
        'patient data not available',
        HttpStatus.BAD_REQUEST,
      );
    } else {
      const bedResult = await this.bedInfoRepository.findOne({
        id: data.bedId,
      });
      if (!bedResult) {
        throw new HttpException(
          'bed info not available',
          HttpStatus.BAD_REQUEST,
        );
      } else {
        const bedAllocationData = new BedAllocation();
        const admitReasonResult = await this.admitReasonRepository.findOne({
          id: data.reasonforadmitId,
        });
        bedAllocationData.admittedReason = admitReasonResult.admitReason;
        bedAllocationData.admitReason = admitReasonResult;
        bedAllocationData.patientInfo = patientResult;
        bedAllocationData.bedInfo = bedResult;
        const newData = Object.assign(bedAllocationData, data);
        return this.bedAllocationRepository.save(newData);
      }
    }
  }

  /**
   * get method for all bed allocation data
   * @returns all bed allocation info
   */
  async getAllBedAllocationInfo(): Promise<BedAllocation[]> {
    return this.bedAllocationRepository.find();
  }

  /**
   * get method based on admitted reason
   * @param admittedReason taking admittedReason
   * @returns based on the reason getting the data
   */
  async getBedAllocationByAdmittedReason(
    admittedReason: string,
  ): Promise<BedAllocation[]> {
    return this.bedAllocationRepository.find({
      admittedReason: admittedReason,
    });
  }

  /**
   * update method for bed allocation info
   * @param id taking which we need to update
   * @param data taking data to update
   * @returns updated information
   */
  async updateBedAllocation(id: number, data: BedAllocation): Promise<string> {
    const info = await this.bedAllocationRepository.findOne({ id: id });
    if (!info) {
      throw new HttpException(
        'bed allocation info not available for this id not passible to update',
        HttpStatus.BAD_REQUEST,
      );
    } else {
      const patientResult = await this.patientInfoRepository.findOne({
        id: data.patientId,
      });
      if (!patientResult) {
        throw new HttpException(
          'patient data not available',
          HttpStatus.BAD_REQUEST,
        );
      } else {
        const bedResult = await this.bedInfoRepository.findOne({
          id: data.bedId,
        });
        if (!bedResult) {
          throw new HttpException(
            'bed info not available',
            HttpStatus.BAD_REQUEST,
          );
        } else {
          const bedAllocationData = new BedAllocation();
          const admitReasonResult = await this.admitReasonRepository.findOne({
            id: data.reasonforadmitId,
          });
          bedAllocationData.admittedReason = admitReasonResult.admitReason;
          bedAllocationData.admitReason = admitReasonResult;
          bedAllocationData.patientInfo = patientResult;
          bedAllocationData.bedInfo = bedResult;
          const newData = Object.assign(bedAllocationData, data);
          await this.bedAllocationRepository.update({ id: id }, newData);
          return 'updated successfully';
        }
      }
    }
  }

  /**
   * delete method
   * @param id taking id which we need to delete
   * @returns deleted information
   */
  async deleteBedAllocationInfo(id: number) {
    return await this.bedAllocationRepository.delete({ id: id });
  }
}
