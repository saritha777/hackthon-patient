import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AdmitReason } from 'src/entity/admitReason.entity';
import { BedInfo } from 'src/entity/bed.entity';
import { BedAllocation } from 'src/entity/bedAllocation.entity';
import { PatientInfo } from 'src/entity/patient.entity';
import { BedAllocationController } from './bedAllocation.controller';
import { BedAllocationService } from './bedAllocation.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      BedInfo,
      BedAllocation,
      AdmitReason,
      PatientInfo,
    ]),
  ],
  controllers: [BedAllocationController],
  providers: [BedAllocationService],
})
export class BedAllocationModule {}
