import {
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { CompoDecorators } from 'src/common/decorators/compodecorator';
import { BedAllocation } from 'src/entity/bedAllocation.entity';
import { BedAllocationService } from './bedAllocation.service';

/**
 * bed allocation service having all the api related bed allocation info
 */
@ApiTags('BedAllocation')
@Controller('/BedAllocation')
export class BedAllocationController {
  /**
   * injecting bedAllocationService for all the business logics
   * @param bedAllocationService injecting bedAllocationService
   */
  constructor(private readonly bedAllocationService: BedAllocationService) {}

  /**
   * add method for bed allocation data
   * @param data taking bedAllocation data
   * @returns saving data to the database
   */
  @Post()
  @CompoDecorators('admin', 'doctor')
  addBedAllocationInfo(@Body() data: BedAllocation): Promise<BedAllocation> {
    return this.bedAllocationService.bedAllocation(data);
  }

  /**
   * get method for all bedAllocation data
   * @returns all info
   */
  @Get()
  getAllBedAllocationInfo() {
    const result = this.bedAllocationService.getAllBedAllocationInfo();
    if (result) {
      return result;
    } else {
      throw new HttpException(
        'bed allocation info not availabe',
        HttpStatus.BAD_REQUEST,
      );
    }
  }

  /**
   * get method based on admitted reason
   * @param admittedReason taking admittedReason
   * @returns based on the reason getting the data
   */
  @Get('admittedReason/:admittedReason')
  getbedAllocationByAdmittedReason(
    @Param('admittedReason') admittedReason: string,
  ) {
    const result =
      this.bedAllocationService.getBedAllocationByAdmittedReason(
        admittedReason,
      );
    if (result) {
      return result;
    } else {
      throw new HttpException(
        'bed allocation info not availabe for this admitted reason',
        HttpStatus.BAD_REQUEST,
      );
    }
  }

  /**
   * update method for all bed allocation
   * @param id taking id which we need to update
   * @param data taking data to update
   * @returns
   */
  @Put('/id/:id')
  @CompoDecorators('admin', 'doctor')
  updateBedAllocationInfo(
    @Param('id') id: number,
    @Body() data: BedAllocation,
  ): Promise<string> {
    return this.bedAllocationService.updateBedAllocation(id, data);
  }

  /**
   * delete based on id
   * @param id taking id to delete
   * @returns deleted info
   */
  @Delete('/:id')
  @CompoDecorators('admin', 'doctor')
  deleteBedInfo(@Param('id') id: number) {
    return this.bedAllocationService.deleteBedAllocationInfo(id);
  }
}
