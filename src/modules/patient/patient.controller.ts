import {
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { CompoDecorators } from 'src/common/decorators/compodecorator';
import { PatientInfo } from 'src/entity/patient.entity';
import { PatientInfoService } from './patient.service';

/**
 * patient controller having all the api related to patient data
 */
@ApiTags('PatientInfo')
@Controller('/PatientInfo')
export class PatientInfoController {
  /**
   * injecting patientInfo service for all business logics
   * @param patientInfoService injecting patient service
   */
  constructor(private readonly patientInfoService: PatientInfoService) {}

  /**
   * add method for patient info
   * @param data taking patient data
   * @returns saving data to database
   */
  @Post()
  @CompoDecorators('receptionist')
  addPatientInfo(@Body() data: PatientInfo): Promise<PatientInfo> {
    return this.patientInfoService.addPatientInfo(data);
  }

  /**
   * get method for all patient info
   * @returns all patients info
   */
  @Get()
  getAllPatientInfo(): Promise<PatientInfo[]> {
    return this.patientInfoService.getAllPatientInfo();
  }

  /**
   * get method for patient based on name
   * @param firstName taking first name
   * @returns all patient info based on name
   */
  @Get('/firstName/:firstName')
  getPatientByFirstName(
    @Param('firstName') firstName: string,
  ): Promise<PatientInfo[]> {
    return this.patientInfoService.getPatientInfoByName(firstName);
  }

  /**
   * get patient info by visit tyoe
   * @param visitType taking visit type
   * @returns
   */
  @Get('/visitType/:visitType')
  getPatientByType(@Param('visitType') visitType: string) {
    return this.patientInfoService.getPatientByType(visitType);
  }

  /**
   * update method for patient info
   * @param id taking id which we need to update
   * @param data taking data for updating
   * @returns updated info
   */
  @Put('/id/:id')
  @CompoDecorators('receptionist')
  updatePatientInfo(@Param('id') id: number, @Body() patientInfo: PatientInfo) {
    return this.patientInfoService.updatePatientInfo(id, patientInfo);
  }

  /**
   * delete method for patient
   * @param patientId taking patient id which we need to delete
   * @returns deleted info
   */
  @Delete('/:patientId')
  @CompoDecorators('receptionist')
  deletePatientInfo(@Param('patientId') patientId: number) {
    return this.patientInfoService.deletePatientInfo(patientId);
  }
}
