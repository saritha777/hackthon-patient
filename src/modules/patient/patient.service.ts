import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { PatientInfo } from 'src/entity/patient.entity';
import { Repository } from 'typeorm';

/**
 * patient info service having all the business logics related to patient info
 */
@Injectable()
export class PatientInfoService {
  /**
   * injecting patientRepository for storing and retriving the data from database
   * @param patientRepository injecting patient repository
   */
  constructor(
    @InjectRepository(PatientInfo)
    private patientRepository: Repository<PatientInfo>,
  ) {}

  /**
   * add method for patient info
   * @param data taking patient data
   * @returns saving data to database
   */
  async addPatientInfo(data: PatientInfo): Promise<PatientInfo> {
    const patientData = new PatientInfo();
    const newData = Object.assign(patientData, data);
    return await this.patientRepository.save(newData);
  }

  /**
   * get method for all patient info
   * @returns all patients info
   */
  async getAllPatientInfo(): Promise<PatientInfo[]> {
    return await this.patientRepository.find();
  }

  /**
   * get method for patient based on name
   * @param firstName taking first name
   * @returns all patient info based on name
   */
  async getPatientInfoByName(firstName: string): Promise<PatientInfo[]> {
    return await this.patientRepository.find({ firstName: firstName });
  }

  /**
   * get patient info by visit tyoe
   * @param visitType taking visit type
   * @returns
   */
  async getPatientByType(visitType: string) {
    return this.patientRepository.find({ visitType: visitType });
  }

  /**
   * update method for patient info
   * @param id taking id which we need to update
   * @param data taking data for updating
   * @returns updated info
   */
  async updatePatientInfo(id: number, data: PatientInfo): Promise<string> {
    const info = await this.patientRepository.findOne({ id: id });
    if (!info) {
      throw new HttpException('data not available', HttpStatus.BAD_REQUEST);
    } else {
      const patientData = new PatientInfo();
      const newData = Object.assign(patientData, data);
      await this.patientRepository.update({ id: id }, newData);
      return 'updated succefully';
    }
  }

  /**
   * delete method for patient
   * @param patientId taking patient id which we need to delete
   * @returns deleted info
   */
  async deletePatientInfo(patientId: number) {
    return await this.patientRepository.delete({ id: patientId });
  }
}
