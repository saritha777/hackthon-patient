import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PatientInfo } from 'src/entity/patient.entity';
import { PatientInfoController } from './patient.controller';
import { PatientInfoService } from './patient.service';

@Module({
  imports: [TypeOrmModule.forFeature([PatientInfo])],
  controllers: [PatientInfoController],
  providers: [PatientInfoService],
})
export class PatientInfoModule {}
