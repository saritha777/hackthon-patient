import { Injectable } from '@nestjs/common';

/**
 * app service
 */
@Injectable()
export class AppService {
  /**
   * get method for hello
   * @returns hello world
   */
  getHello(): string {
    return 'Hello World!';
  }
}
