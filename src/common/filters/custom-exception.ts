import { HttpException, HttpStatus } from '@nestjs/common';

/**
 * startting point of unauthorized exception
 */
export class UnauthorizedException extends HttpException {
  /**
   * extending form httpException having super method
   */
  constructor() {
    super('Invalid User', HttpStatus.UNAUTHORIZED);
  }
}

/**
 * starting point of user not found exception
 */
export class UserNotFoundException extends HttpException {
  /**
   * extending form httpException having super method
   */
  constructor() {
    super('User Not Found ,please log in to continue', HttpStatus.NOT_FOUND);
  }
}

/**
 * starting point of wrong crendentials exception
 */
export class WrongCredentialsException extends HttpException {
  /**
   * extending form httpException having super method
   */
  constructor() {
    super('Invalid Credentials', HttpStatus.NOT_ACCEPTABLE);
  }
}

/**
 * starting point of data not found exception
 */
export class DataNotFoundException extends HttpException {
  /**
   * extending form httpException having super method
   */
  constructor() {
    super('No data found', HttpStatus.NOT_FOUND);
  }
}

/**
 * starting point of authentication exception
 */
export class AuthenticationException extends HttpException {
  /**
   * extending form httpException having super method
   */
  constructor() {
    super('Authentication error', HttpStatus.UNAUTHORIZED);
  }
}
