import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  HttpException,
} from '@nestjs/common';
import { Request, Response } from 'express';
import { Logger } from 'src/config/logger/logger.service';

/**
 * writting catch method to cought the exceptions
 */
@Catch(HttpException)
/**
 * writting a class for http exception filter
 */
export class HttpExceptionFilter implements ExceptionFilter {
  /**
   * creating logger object
   */
  private loggerService: Logger;
  /**
   * writting catch methot to cought
   * @param exception passing exception as input
   * @param host passing as imput
   */

  catch(exception: HttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const request = ctx.getRequest<Request>();
    const status: any = exception.getStatus();
    this.loggerService.error('', status);

    response.status(status).json({
      statusCode: status,
      timestamp: new Date().toISOString(),
      path: request.url,
      description: 'Http error',
    });
  }
}
