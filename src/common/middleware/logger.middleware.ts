import { Injectable, Logger, NestMiddleware } from '@nestjs/common';
import { NextFunction } from 'express';
import { Request, Response } from 'express';

/**
 * logger midelteware implemetnting from nest middeleware
 */
@Injectable()
export class LoggerMiddleware implements NestMiddleware {
  /**
   * we create logger instance
   */
  private loggerService = Logger;
  /**
   *transfer the next route folder
   * @param request sending request
   * @param response taking response
   * @param next takes param as next to move next function
   */
  use(request: Request, response: Response, next: NextFunction): void {
    const { ip, method, originalUrl } = request;
    const userAgent = request.get('user-agent') || '';

    response.on('finish', () => {
      const { statusCode } = response;
      const contentLenght = response.get('content-lenght');
      this.loggerService.log(
        `${method} ${originalUrl} ${statusCode} ${contentLenght} -${userAgent} ${ip}`,
      );
    });

    next();
  }
}
