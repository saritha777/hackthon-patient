import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';
import { Maintainance } from 'src/entity/maintainance';

/**
 * user dto
 */
export class UserDto extends Maintainance {
  /**
   * name property
   */
  @ApiProperty()
  @IsString()
  name: string;

  /**
   * email property
   */
  @ApiProperty()
  @IsString()
  email: string;

  /**
   * password property
   */
  @ApiProperty()
  @IsString()
  password: string;

  /**
   * phone no property
   */
  @ApiProperty()
  @IsString()
  phoneNo: string;

  /**
   * role property
   */
  @ApiProperty()
  @IsString()
  role: string;
}
