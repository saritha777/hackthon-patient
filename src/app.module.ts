import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { LoggerMiddleware } from './common/middleware/logger.middleware';
import { BedInfoModule } from './modules/bed/bed.module';
import { BedAllocationModule } from './modules/bedAllocation/bedAllocation.module';
import { ConfigurationModule } from './modules/configurationTables/configuration.module';
import { PatientInfoModule } from './modules/patient/patient.module';

import { UserModule } from './modules/user/user.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: 'root',
      database: 'patient_mgmt',
      autoLoadEntities: true,
      synchronize: true,
    }),
    UserModule,
    ConfigurationModule,
    PatientInfoModule,
    BedInfoModule,
    BedAllocationModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(LoggerMiddleware).forRoutes('*');
  }
}
