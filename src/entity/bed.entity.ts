import { ApiProperty } from '@nestjs/swagger';
import { IsInt } from 'class-validator';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { BedAllocation } from './bedAllocation.entity';
import { RoomCategory } from './roomCategory.entity';
import { RoomStatus } from './roomStatus.entity';

/**
 * bed info entity
 */
@Entity()
export class BedInfo {
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @IsInt()
  @Column()
  roomCategoryId: number;

  @ApiProperty()
  @IsInt()
  @Column()
  roomStatusId: number;

  @ApiProperty()
  @IsInt()
  @Column()
  floorNumber: number;

  @ApiProperty()
  @IsInt()
  @Column()
  RoomNumber: number;

  @ApiProperty()
  @IsInt()
  @Column()
  noOfOccupents: number;

  @ApiProperty()
  @IsInt()
  @Column()
  noOfCoOccupents: number;

  @ManyToOne(() => RoomCategory, (roomCategory) => roomCategory, {
    cascade: true,
  })
  @JoinColumn()
  roomCategory: RoomCategory;

  @ManyToOne(() => RoomStatus, (roomStatus) => roomStatus.bedInfo, {
    cascade: true,
  })
  roomStatus: RoomStatus;
  @OneToMany(() => BedAllocation, (bedAllocation) => bedAllocation.bedInfo, {
    cascade: true,
  })
  @JoinColumn()
  bedAllocation: BedAllocation[];
}
