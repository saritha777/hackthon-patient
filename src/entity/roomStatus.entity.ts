import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';
import {
  Column,
  Entity,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { BedInfo } from './bed.entity';

/**
 * room status entity
 */
@Entity()
export class RoomStatus {
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @IsString()
  @Column()
  roomStatus: string;

  @OneToMany(() => BedInfo, (bedInfo) => bedInfo.roomStatus)
  bedInfo: BedInfo;
}
