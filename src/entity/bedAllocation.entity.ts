import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsString } from 'class-validator';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { AdmitReason } from './admitReason.entity';
import { BedInfo } from './bed.entity';
import { PatientInfo } from './patient.entity';

/**
 * bed allocation info
 */
@Entity()
export class BedAllocation {
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @IsInt()
  @Column()
  patientId: number;

  @ApiProperty()
  @IsInt()
  @Column()
  bedId: number;

  @ApiProperty()
  @IsString()
  @Column()
  fromDate: string;

  @ApiProperty()
  @IsString()
  @Column()
  toDate: string;

  @ApiProperty()
  @IsInt()
  @Column()
  reasonforadmitId: number;

  @Column()
  admittedReason: string;

  @ManyToOne(() => PatientInfo, (patientInfo) => patientInfo.bedAllocation)
  patientInfo: PatientInfo;

  @ManyToOne(() => BedInfo, (bedInfo) => bedInfo.bedAllocation)
  bedInfo: BedInfo;

  @ManyToOne(() => AdmitReason, (admitReason) => admitReason.bedAllocation)
  admitReason: AdmitReason;
}
