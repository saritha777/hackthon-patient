import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';
import {
  Column,
  Entity,
  ManyToMany,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { BedInfo } from './bed.entity';

/**
 * room category entity
 */
@Entity()
export class RoomCategory {
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @IsString()
  @Column()
  roomCategory: string;

  @OneToMany(() => BedInfo, (bedInfo) => bedInfo.roomCategory)
  bedInfo: BedInfo;
}
