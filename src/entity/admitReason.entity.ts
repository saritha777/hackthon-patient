import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';
import {
  Column,
  Entity,
  JoinColumn,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { BedAllocation } from './bedAllocation.entity';

/**
 * admit reason entity
 */
@Entity()
export class AdmitReason {
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @IsString()
  @Column()
  admitReason: string;

  @OneToMany(
    () => BedAllocation,
    (bedAllocation) => bedAllocation.admitReason,
    {
      cascade: true,
    },
  )
  @JoinColumn()
  bedAllocation: BedAllocation[];
}
