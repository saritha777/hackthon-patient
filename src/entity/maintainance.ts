import { ApiProperty } from '@nestjs/swagger';
import { IsString, IsBoolean } from 'class-validator';
import { Column } from 'typeorm';
/**
 * creating entity for table creation
 */
export class Maintainance {
  /**
   * declaring isactive
   */
  @IsBoolean()
  @ApiProperty()
  @Column('boolean', { default: true })
  isActive: boolean;
  /**
   * declaring updatedBy
   */
  @IsString()
  @ApiProperty()
  @Column()
  updatedBy: string;
  /**
   * declaring updatedDate
   */
  @IsString()
  @ApiProperty()
  @Column()
  updatedDate: string;
  /**
   * declaring createdBy
   */
  @IsString()
  @ApiProperty()
  @Column()
  createdBy: string;
  /**
   * declaring createdDate
   */
  @IsString()
  @ApiProperty()
  @Column()
  createdDate: string;

  // @BeforeInsert()
  // async changeDate() {
  //   this.createdDate = moment().format('MMM Do YY');
  //   this.updatedDate = moment().format('MMM Do YY');
  // }
}
