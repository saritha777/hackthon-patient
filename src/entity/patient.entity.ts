import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';
import {
  Column,
  Entity,
  JoinColumn,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { BedAllocation } from './bedAllocation.entity';

/**
 * patientInfo entity
 */
@Entity()
export class PatientInfo {
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @IsString()
  @Column()
  firstName: string;

  @ApiProperty()
  @IsString()
  @Column()
  lastname: string;

  @ApiProperty()
  @IsString()
  @Column()
  DoB: string;

  @ApiProperty()
  @IsString()
  @Column()
  gender: string;

  @ApiProperty()
  @IsString()
  @Column()
  phoneNo: string;

  @ApiProperty()
  @IsString()
  @Column()
  email: string;

  @ApiProperty()
  @IsString()
  @Column()
  addressLine1: string;

  @ApiProperty()
  @IsString()
  @Column()
  AddressLine2: string;

  @ApiProperty()
  @IsString()
  @Column()
  city: string;

  @ApiProperty()
  @IsString()
  @Column()
  state: string;

  @ApiProperty()
  @IsString()
  @Column()
  country: string;

  @ApiProperty()
  @IsString()
  @Column()
  pincode: string;

  @ApiProperty()
  @IsString()
  @Column()
  visitType: string;

  @OneToMany(
    () => BedAllocation,
    (bedAllocation) => bedAllocation.patientInfo,
    {
      cascade: true,
    },
  )
  @JoinColumn()
  bedAllocation: BedAllocation[];
}
