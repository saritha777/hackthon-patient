import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';
import { Column, Entity, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { UserLogin } from './login.entity';
import { Maintainance } from './maintainance';

/**
 * user entity
 */
@Entity()
export class User extends Maintainance {
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @IsString()
  @Column()
  name: string;

  @ApiProperty()
  @IsString()
  @Column()
  email: string;

  @ApiProperty()
  @IsString()
  @Column()
  password: string;

  @ApiProperty()
  @IsString()
  @Column()
  phoneNo: string;

  @ApiProperty()
  @IsString()
  @Column()
  role: string;

  @OneToOne(() => UserLogin, (login) => login.user, {
    cascade: true,
  })
  login: UserLogin;
}
