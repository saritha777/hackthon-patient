import { Injectable, LoggerService } from '@nestjs/common';

/**
 * logger class implementing from loggerservice
 */
@Injectable()
export class Logger implements LoggerService {
  /**
   * log method will print log on console
   * @param message passing message
   */
  log(message: string) {
    console.log(message);
  }
  /**
   * error method will print error in console
   * @param message passing messsage
   * @param trace pasiing status code
   */
  error(message: string, trace: string) {
    console.error(message);
  }

  /**
   * warn method will print warning in console
   * @param message passing message
   */
  warn(message: string) {
    console.warn(message);
  }
}
