import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';

/**
 * app controller
 */
@Controller()
export class AppController {
  /**
   * injecting app service for business logics
   * @param appService injecting app service
   */
  constructor(private readonly appService: AppService) {}

  /**
   * get method for hello
   * @returns hello world
   */
  @Get()
  getHello(): string {
    return this.appService.getHello();
  }
}
