'use strict';

customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">hockathon documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link" >AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-AppModule-79a0e873e5a3255a72e62dadc0b366c4194a494bdfcb9fbfa5c4fc662690b41a62ded166c3c213a21099a515ab4d084b83dfe6d06eed1f3475c2346821dc7c45"' : 'data-target="#xs-controllers-links-module-AppModule-79a0e873e5a3255a72e62dadc0b366c4194a494bdfcb9fbfa5c4fc662690b41a62ded166c3c213a21099a515ab4d084b83dfe6d06eed1f3475c2346821dc7c45"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-AppModule-79a0e873e5a3255a72e62dadc0b366c4194a494bdfcb9fbfa5c4fc662690b41a62ded166c3c213a21099a515ab4d084b83dfe6d06eed1f3475c2346821dc7c45"' :
                                            'id="xs-controllers-links-module-AppModule-79a0e873e5a3255a72e62dadc0b366c4194a494bdfcb9fbfa5c4fc662690b41a62ded166c3c213a21099a515ab4d084b83dfe6d06eed1f3475c2346821dc7c45"' }>
                                            <li class="link">
                                                <a href="controllers/AppController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-AppModule-79a0e873e5a3255a72e62dadc0b366c4194a494bdfcb9fbfa5c4fc662690b41a62ded166c3c213a21099a515ab4d084b83dfe6d06eed1f3475c2346821dc7c45"' : 'data-target="#xs-injectables-links-module-AppModule-79a0e873e5a3255a72e62dadc0b366c4194a494bdfcb9fbfa5c4fc662690b41a62ded166c3c213a21099a515ab4d084b83dfe6d06eed1f3475c2346821dc7c45"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-AppModule-79a0e873e5a3255a72e62dadc0b366c4194a494bdfcb9fbfa5c4fc662690b41a62ded166c3c213a21099a515ab4d084b83dfe6d06eed1f3475c2346821dc7c45"' :
                                        'id="xs-injectables-links-module-AppModule-79a0e873e5a3255a72e62dadc0b366c4194a494bdfcb9fbfa5c4fc662690b41a62ded166c3c213a21099a515ab4d084b83dfe6d06eed1f3475c2346821dc7c45"' }>
                                        <li class="link">
                                            <a href="injectables/AppService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/BedAllocationModule.html" data-type="entity-link" >BedAllocationModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-BedAllocationModule-c9436153260c940100638380c6b3fc5b2d4806bd64ad9e2c6be3ca95840af3cf2119eace46428d492b95b3a38d4596c171eed75f3290a361d7e3d9c671b26f67"' : 'data-target="#xs-controllers-links-module-BedAllocationModule-c9436153260c940100638380c6b3fc5b2d4806bd64ad9e2c6be3ca95840af3cf2119eace46428d492b95b3a38d4596c171eed75f3290a361d7e3d9c671b26f67"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-BedAllocationModule-c9436153260c940100638380c6b3fc5b2d4806bd64ad9e2c6be3ca95840af3cf2119eace46428d492b95b3a38d4596c171eed75f3290a361d7e3d9c671b26f67"' :
                                            'id="xs-controllers-links-module-BedAllocationModule-c9436153260c940100638380c6b3fc5b2d4806bd64ad9e2c6be3ca95840af3cf2119eace46428d492b95b3a38d4596c171eed75f3290a361d7e3d9c671b26f67"' }>
                                            <li class="link">
                                                <a href="controllers/BedAllocationController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >BedAllocationController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-BedAllocationModule-c9436153260c940100638380c6b3fc5b2d4806bd64ad9e2c6be3ca95840af3cf2119eace46428d492b95b3a38d4596c171eed75f3290a361d7e3d9c671b26f67"' : 'data-target="#xs-injectables-links-module-BedAllocationModule-c9436153260c940100638380c6b3fc5b2d4806bd64ad9e2c6be3ca95840af3cf2119eace46428d492b95b3a38d4596c171eed75f3290a361d7e3d9c671b26f67"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-BedAllocationModule-c9436153260c940100638380c6b3fc5b2d4806bd64ad9e2c6be3ca95840af3cf2119eace46428d492b95b3a38d4596c171eed75f3290a361d7e3d9c671b26f67"' :
                                        'id="xs-injectables-links-module-BedAllocationModule-c9436153260c940100638380c6b3fc5b2d4806bd64ad9e2c6be3ca95840af3cf2119eace46428d492b95b3a38d4596c171eed75f3290a361d7e3d9c671b26f67"' }>
                                        <li class="link">
                                            <a href="injectables/BedAllocationService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >BedAllocationService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/BedInfoModule.html" data-type="entity-link" >BedInfoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-BedInfoModule-aa0a1d8daacf536b2325649dbada5e275ae45f2825bda7806d8136e18c8e1ce87d57fc8d68e582d732cbecead3484e03cc61301499c6665c51e702a128187298"' : 'data-target="#xs-controllers-links-module-BedInfoModule-aa0a1d8daacf536b2325649dbada5e275ae45f2825bda7806d8136e18c8e1ce87d57fc8d68e582d732cbecead3484e03cc61301499c6665c51e702a128187298"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-BedInfoModule-aa0a1d8daacf536b2325649dbada5e275ae45f2825bda7806d8136e18c8e1ce87d57fc8d68e582d732cbecead3484e03cc61301499c6665c51e702a128187298"' :
                                            'id="xs-controllers-links-module-BedInfoModule-aa0a1d8daacf536b2325649dbada5e275ae45f2825bda7806d8136e18c8e1ce87d57fc8d68e582d732cbecead3484e03cc61301499c6665c51e702a128187298"' }>
                                            <li class="link">
                                                <a href="controllers/BedInfoController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >BedInfoController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-BedInfoModule-aa0a1d8daacf536b2325649dbada5e275ae45f2825bda7806d8136e18c8e1ce87d57fc8d68e582d732cbecead3484e03cc61301499c6665c51e702a128187298"' : 'data-target="#xs-injectables-links-module-BedInfoModule-aa0a1d8daacf536b2325649dbada5e275ae45f2825bda7806d8136e18c8e1ce87d57fc8d68e582d732cbecead3484e03cc61301499c6665c51e702a128187298"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-BedInfoModule-aa0a1d8daacf536b2325649dbada5e275ae45f2825bda7806d8136e18c8e1ce87d57fc8d68e582d732cbecead3484e03cc61301499c6665c51e702a128187298"' :
                                        'id="xs-injectables-links-module-BedInfoModule-aa0a1d8daacf536b2325649dbada5e275ae45f2825bda7806d8136e18c8e1ce87d57fc8d68e582d732cbecead3484e03cc61301499c6665c51e702a128187298"' }>
                                        <li class="link">
                                            <a href="injectables/BedInfoService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >BedInfoService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/ConfigurationModule.html" data-type="entity-link" >ConfigurationModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-ConfigurationModule-24693664e48d24eb01d8e0dd4c2a8c977921cc9cbe8446eee4dc98faabf6f374da28bf309fb28d00a3f46b8b6677ba2986426c617067372826c60bd98c1de071"' : 'data-target="#xs-controllers-links-module-ConfigurationModule-24693664e48d24eb01d8e0dd4c2a8c977921cc9cbe8446eee4dc98faabf6f374da28bf309fb28d00a3f46b8b6677ba2986426c617067372826c60bd98c1de071"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-ConfigurationModule-24693664e48d24eb01d8e0dd4c2a8c977921cc9cbe8446eee4dc98faabf6f374da28bf309fb28d00a3f46b8b6677ba2986426c617067372826c60bd98c1de071"' :
                                            'id="xs-controllers-links-module-ConfigurationModule-24693664e48d24eb01d8e0dd4c2a8c977921cc9cbe8446eee4dc98faabf6f374da28bf309fb28d00a3f46b8b6677ba2986426c617067372826c60bd98c1de071"' }>
                                            <li class="link">
                                                <a href="controllers/ConfigurationController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ConfigurationController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-ConfigurationModule-24693664e48d24eb01d8e0dd4c2a8c977921cc9cbe8446eee4dc98faabf6f374da28bf309fb28d00a3f46b8b6677ba2986426c617067372826c60bd98c1de071"' : 'data-target="#xs-injectables-links-module-ConfigurationModule-24693664e48d24eb01d8e0dd4c2a8c977921cc9cbe8446eee4dc98faabf6f374da28bf309fb28d00a3f46b8b6677ba2986426c617067372826c60bd98c1de071"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-ConfigurationModule-24693664e48d24eb01d8e0dd4c2a8c977921cc9cbe8446eee4dc98faabf6f374da28bf309fb28d00a3f46b8b6677ba2986426c617067372826c60bd98c1de071"' :
                                        'id="xs-injectables-links-module-ConfigurationModule-24693664e48d24eb01d8e0dd4c2a8c977921cc9cbe8446eee4dc98faabf6f374da28bf309fb28d00a3f46b8b6677ba2986426c617067372826c60bd98c1de071"' }>
                                        <li class="link">
                                            <a href="injectables/ConfigurationService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ConfigurationService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/PatientInfoModule.html" data-type="entity-link" >PatientInfoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-PatientInfoModule-7341d5608e96571921140fd2d9656cbc000eeb7fdc7d2c7351964074182b00c61d08a5eb899104f56f56d36340fdbcfba6ec920de477afe3464a99dbb425e9d5"' : 'data-target="#xs-controllers-links-module-PatientInfoModule-7341d5608e96571921140fd2d9656cbc000eeb7fdc7d2c7351964074182b00c61d08a5eb899104f56f56d36340fdbcfba6ec920de477afe3464a99dbb425e9d5"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-PatientInfoModule-7341d5608e96571921140fd2d9656cbc000eeb7fdc7d2c7351964074182b00c61d08a5eb899104f56f56d36340fdbcfba6ec920de477afe3464a99dbb425e9d5"' :
                                            'id="xs-controllers-links-module-PatientInfoModule-7341d5608e96571921140fd2d9656cbc000eeb7fdc7d2c7351964074182b00c61d08a5eb899104f56f56d36340fdbcfba6ec920de477afe3464a99dbb425e9d5"' }>
                                            <li class="link">
                                                <a href="controllers/PatientInfoController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PatientInfoController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-PatientInfoModule-7341d5608e96571921140fd2d9656cbc000eeb7fdc7d2c7351964074182b00c61d08a5eb899104f56f56d36340fdbcfba6ec920de477afe3464a99dbb425e9d5"' : 'data-target="#xs-injectables-links-module-PatientInfoModule-7341d5608e96571921140fd2d9656cbc000eeb7fdc7d2c7351964074182b00c61d08a5eb899104f56f56d36340fdbcfba6ec920de477afe3464a99dbb425e9d5"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-PatientInfoModule-7341d5608e96571921140fd2d9656cbc000eeb7fdc7d2c7351964074182b00c61d08a5eb899104f56f56d36340fdbcfba6ec920de477afe3464a99dbb425e9d5"' :
                                        'id="xs-injectables-links-module-PatientInfoModule-7341d5608e96571921140fd2d9656cbc000eeb7fdc7d2c7351964074182b00c61d08a5eb899104f56f56d36340fdbcfba6ec920de477afe3464a99dbb425e9d5"' }>
                                        <li class="link">
                                            <a href="injectables/PatientInfoService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PatientInfoService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/UserModule.html" data-type="entity-link" >UserModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-UserModule-2f45c95f49ef7685f36ab15c3bb591bc45be2fa2437d46b9c88f55682bbc649c2cc76b8670352035c4dfdccde2a1d6061b914345e219ccb0b76edf343b7db923"' : 'data-target="#xs-controllers-links-module-UserModule-2f45c95f49ef7685f36ab15c3bb591bc45be2fa2437d46b9c88f55682bbc649c2cc76b8670352035c4dfdccde2a1d6061b914345e219ccb0b76edf343b7db923"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-UserModule-2f45c95f49ef7685f36ab15c3bb591bc45be2fa2437d46b9c88f55682bbc649c2cc76b8670352035c4dfdccde2a1d6061b914345e219ccb0b76edf343b7db923"' :
                                            'id="xs-controllers-links-module-UserModule-2f45c95f49ef7685f36ab15c3bb591bc45be2fa2437d46b9c88f55682bbc649c2cc76b8670352035c4dfdccde2a1d6061b914345e219ccb0b76edf343b7db923"' }>
                                            <li class="link">
                                                <a href="controllers/UserController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >UserController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-UserModule-2f45c95f49ef7685f36ab15c3bb591bc45be2fa2437d46b9c88f55682bbc649c2cc76b8670352035c4dfdccde2a1d6061b914345e219ccb0b76edf343b7db923"' : 'data-target="#xs-injectables-links-module-UserModule-2f45c95f49ef7685f36ab15c3bb591bc45be2fa2437d46b9c88f55682bbc649c2cc76b8670352035c4dfdccde2a1d6061b914345e219ccb0b76edf343b7db923"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-UserModule-2f45c95f49ef7685f36ab15c3bb591bc45be2fa2437d46b9c88f55682bbc649c2cc76b8670352035c4dfdccde2a1d6061b914345e219ccb0b76edf343b7db923"' :
                                        'id="xs-injectables-links-module-UserModule-2f45c95f49ef7685f36ab15c3bb591bc45be2fa2437d46b9c88f55682bbc649c2cc76b8670352035c4dfdccde2a1d6061b914345e219ccb0b76edf343b7db923"' }>
                                        <li class="link">
                                            <a href="injectables/JwtToken.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >JwtToken</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/UserService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >UserService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                </ul>
                </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#controllers-links"' :
                                'data-target="#xs-controllers-links"' }>
                                <span class="icon ion-md-swap"></span>
                                <span>Controllers</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="controllers-links"' : 'id="xs-controllers-links"' }>
                                <li class="link">
                                    <a href="controllers/AppController.html" data-type="entity-link" >AppController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/BedAllocationController.html" data-type="entity-link" >BedAllocationController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/BedInfoController.html" data-type="entity-link" >BedInfoController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/ConfigurationController.html" data-type="entity-link" >ConfigurationController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/PatientInfoController.html" data-type="entity-link" >PatientInfoController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/UserController.html" data-type="entity-link" >UserController</a>
                                </li>
                            </ul>
                        </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#entities-links"' :
                                'data-target="#xs-entities-links"' }>
                                <span class="icon ion-ios-apps"></span>
                                <span>Entities</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="entities-links"' : 'id="xs-entities-links"' }>
                                <li class="link">
                                    <a href="entities/AdmitReason.html" data-type="entity-link" >AdmitReason</a>
                                </li>
                                <li class="link">
                                    <a href="entities/BedAllocation.html" data-type="entity-link" >BedAllocation</a>
                                </li>
                                <li class="link">
                                    <a href="entities/BedInfo.html" data-type="entity-link" >BedInfo</a>
                                </li>
                                <li class="link">
                                    <a href="entities/PatientInfo.html" data-type="entity-link" >PatientInfo</a>
                                </li>
                                <li class="link">
                                    <a href="entities/RoomCategory.html" data-type="entity-link" >RoomCategory</a>
                                </li>
                                <li class="link">
                                    <a href="entities/RoomStatus.html" data-type="entity-link" >RoomStatus</a>
                                </li>
                                <li class="link">
                                    <a href="entities/User.html" data-type="entity-link" >User</a>
                                </li>
                                <li class="link">
                                    <a href="entities/UserLogin.html" data-type="entity-link" >UserLogin</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#classes-links"' :
                            'data-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/AuthenticationException.html" data-type="entity-link" >AuthenticationException</a>
                            </li>
                            <li class="link">
                                <a href="classes/DataNotFoundException.html" data-type="entity-link" >DataNotFoundException</a>
                            </li>
                            <li class="link">
                                <a href="classes/globalAccess.html" data-type="entity-link" >globalAccess</a>
                            </li>
                            <li class="link">
                                <a href="classes/HttpExceptionFilter.html" data-type="entity-link" >HttpExceptionFilter</a>
                            </li>
                            <li class="link">
                                <a href="classes/LoginDto.html" data-type="entity-link" >LoginDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/Maintainance.html" data-type="entity-link" >Maintainance</a>
                            </li>
                            <li class="link">
                                <a href="classes/UnauthorizedException.html" data-type="entity-link" >UnauthorizedException</a>
                            </li>
                            <li class="link">
                                <a href="classes/UserDto.html" data-type="entity-link" >UserDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/UserNotFoundException.html" data-type="entity-link" >UserNotFoundException</a>
                            </li>
                            <li class="link">
                                <a href="classes/ValidationExceptionFilter.html" data-type="entity-link" >ValidationExceptionFilter</a>
                            </li>
                            <li class="link">
                                <a href="classes/WrongCredentialsException.html" data-type="entity-link" >WrongCredentialsException</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/AppService.html" data-type="entity-link" >AppService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/BedAllocationService.html" data-type="entity-link" >BedAllocationService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/BedInfoService.html" data-type="entity-link" >BedInfoService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/CacheManagerInterceptor.html" data-type="entity-link" >CacheManagerInterceptor</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ConfigurationService.html" data-type="entity-link" >ConfigurationService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/JwtToken.html" data-type="entity-link" >JwtToken</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/Logger.html" data-type="entity-link" >Logger</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/LoggerMiddleware.html" data-type="entity-link" >LoggerMiddleware</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/PatientInfoService.html" data-type="entity-link" >PatientInfoService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/UserService.html" data-type="entity-link" >UserService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ValidationPipes.html" data-type="entity-link" >ValidationPipes</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#guards-links"' :
                            'data-target="#xs-guards-links"' }>
                            <span class="icon ion-ios-lock"></span>
                            <span>Guards</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="guards-links"' : 'id="xs-guards-links"' }>
                            <li class="link">
                                <a href="guards/RolesGuard.html" data-type="entity-link" >RolesGuard</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interfaces-links"' :
                            'data-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/SwaggerConfig.html" data-type="entity-link" >SwaggerConfig</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/functions.html" data-type="entity-link">Functions</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});